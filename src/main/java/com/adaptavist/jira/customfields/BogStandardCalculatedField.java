package com.adaptavist.jira.customfields;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.customfields.converters.StringConverter;
import java.util.Map;

@Scanned
public class BogStandardCalculatedField extends CalculatedCFType {
    private static final Logger log = LoggerFactory.getLogger(BogStandardCalculatedField.class);
    @ComponentImport private final StringConverter stringConverter;

    public BogStandardCalculatedField(StringConverter stringConverter) {
        super();
        this.stringConverter = stringConverter;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        //add what you need to the map here

        return map;
    }

    public Object getValueFromIssue(CustomField field, Issue issue) {
        log.warn("Getting last commenter from issue " + issue.getKey());
        ApplicationUser lastUser = null;
        try {
            CommentManager commentManager = ComponentAccessor.getCommentManager();
            Comment lastComment = commentManager.getLastComment(issue);
            if (lastComment != null) {
                lastUser = lastComment.getAuthorApplicationUser();
                log.warn("The last commenter was " + lastUser.getName());
            }
        } catch (Exception e) {
            log.error("It all went horribly wrong", e);
        }

        return lastUser;
    }

    public String getStringFromSingularObject(Object o) {
        return o.toString();
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException {
        return stringConverter.getObject(s);
    }
}