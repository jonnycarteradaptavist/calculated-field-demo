package ut.com.adaptavist.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.adaptavist.jira.customfields.BogStandardCalculatedField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class BogStandardCalculatedFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //BogStandardCalculatedField testClass = new BogStandardCalculatedField();

        throw new Exception("BogStandardCalculatedField has no tests!");

    }

}
