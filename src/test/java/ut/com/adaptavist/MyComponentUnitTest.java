package ut.com.adaptavist;

import org.junit.Test;
import com.adaptavist.api.MyPluginComponent;
import com.adaptavist.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}